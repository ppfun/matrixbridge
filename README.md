# pixelplanet.fun matrix bridge

A bridge from pixelplanet to synapse-matrix homeserver.

Users can login with their pixelplanet credentials on matrix and public chats are connected between the game and matrix.

`./synapse-auth-module` is for authentification and `./ppfun-bridge` for bridging the chats. Look into those folders and read their README.md to see how it works
