"use strict";

import { Logger, Bridge } from 'matrix-appservice-bridge';

import parseMsg from './markdown/parse.js'
import PPfunSocket from './ppfunsocket.js';
import { parseCanvasLinks } from './pixelplanet/index.js';

/*
 * split a string into multiple lines cutting it
 * at character limits
 * @param string
 * @return array of lines
 */
function splitString(string) {
  const lines = [];
  if (!string) return lines;
  // mark start of last word
  let wordEnd = 0;
  // mark start of current line
  let sectionStart = 0;
  // characters allowed per line
  let it = 0;
  while (it < string.length) {
    let chr = string[it];

    if (chr === '\n') {
      if (sectionStart !== it) {
        let end = it;
        while (string[end - 1] === ' ') end -= 1;
        lines.push(string.substring(sectionStart, end));
        if (lines.length >= 5) break;
      }
      sectionStart = it + 1;
    } else if (chr === ' ') {
      // trim spaces at start
      if (it === sectionStart) sectionStart += 1;
      wordEnd = it;
    } else if (it - sectionStart >= 200) {
      // max amount of characters per line reached
      const wordStart = wordEnd + 1;
      while (string[wordEnd - 1] === ' ') wordEnd -= 1;
      // cut line at word or at limit
      if (wordEnd > sectionStart) {
        lines.push(string.substring(sectionStart, wordEnd));
        sectionStart = wordStart;
      } else {
        lines.push(string.substring(sectionStart, it - 1) + '-');
        sectionStart = it - 1;
      }
      if (lines.length >= 5) break;
    }
    it += 1;
  }

  if (it === string.length) {
    while (string[it - 1] === ' ' || string[it - 1] === '\n') it -= 1;
    if (it > sectionStart) lines.push(string.substring(sectionStart, it));
  } else {
    const lastIndex = lines.length - 1;
    lines[lastIndex] = lines[lastIndex].substring(0, 197) + '...';
  }

  return lines;
}

class PPfunMatrixBridge {
  constructor(opts) {
    const {
      apiSocketKey,
      apiSocketUrl,
      homeserverUrl,
      mediaUrl,
      domain,
      registration,
      port
    } = opts;
    this.isConnected = false;
    this.syncChannels = true;
    this.mToProomMap = new Map();
    this.pToMroomMap = new Map();
    this.idToNameMap = new Map();
    // echos only hae to be surpress on the matrix side,
    // because ppfun apisocket doesn't return your own sent messages
    // to you
    this.echoSuppression = new Map();
    this.ppfunSocket = new PPfunSocket(
      apiSocketUrl,
      apiSocketKey,
    );
    this.matrixBridge = new Bridge({
      homeserverUrl,
      domain,
      registration,
      controller: {
        // auto-provision users with no additional data
        onUserQuery: (queriedUser) => {},
        onEvent: this.recMatrix.bind(this),
      }
    });
    Logger.configure({ console: "off", json: false });
    this.port = port;
    this.domain = domain;
    this.mediaUrl = mediaUrl;
    this.prefix = 'pp';

    this.ppfunSocket.on('chanList', this.connectRooms.bind(this));
    this.ppfunSocket.on('chatMessage', this.recPPfun.bind(this));
    this.clearEchoSuppression = this.clearEchoSuppression.bind(this);

    setInterval(this.clearEchoSuppression, 5 * 60 * 1000);
  }
  
  clearEchoSuppression() {
    const curTime = Date.now();
    const arr = Array.from(this.echoSuppression);
    for (let i = 0; i < arr.length; i += 1) {
      const [key, value] = arr[i];
      if (curTime - 60000 > value[1]) {
        this.echoSuppression.delete(key);
      }
    }
  }

  addToEchoSuppression(uid, cid) {
    const key = `${uid}:${cid}`;
    const curTime = Date.now();
    const uidMsgs = this.echoSuppression.get(key);
    if (!uidMsgs) {
      this.echoSuppression.set(key, [1, curTime]);
      return;
    }
    uidMsgs[0] += 1;
    uidMsgs[1] = curTime;
  }

  checkEchoSuppression(uid, cid) {
    const uidMsgs = this.echoSuppression.get(`${uid}:${cid}`);
    if (uidMsgs && uidMsgs[0] > 0) {
      uidMsgs[0] -= 1;
      return true;
    }
    return false;
  }

  recPPfun(name, uid, msg, cid) {
    const matrixRoom = this.pToMroomMap.get(cid);
    if (!matrixRoom) {
      if (!this.syncChannels) {
        console.warn(`Message on unknown channel ${cid}| ${name}: ${msg}`);
        this.ppfunSocket.reqChannels();
      }
      console.warn(`Dropping a message because channels aren't synced yet`);
      return;
    }
    const parsedMsg = parseMsg(msg);
    this.addToEchoSuppression(uid, cid);
    console.log(`PPFUN ${name}: ${parsedMsg}`);
    this.sendMatrix(
      name,
      `@${this.prefix}_${uid}:${this.domain}`,
      parsedMsg,
      matrixRoom,
    );
  }

  async sendCanvasSnapshotIfNeccessary(roomId, msg) {
    // post image if canvas link got posted
    try {
      const imageData = await parseCanvasLinks(msg);
      if (imageData) {
        const { image, name, type, h, w, size } = imageData;
        const intent = this.matrixBridge.getIntent();
        let mxcUrl;
        mxcUrl = await intent.uploadContent(image, {
          name,
          type,
        });
        if (mxcUrl) {
          intent.sendMessage(roomId, {
            body: name,
            msgtype: 'm.image',
            url: mxcUrl,
            info: {
              h,
              w,
              mimetype: type,
              size,
            },
          });
        }
      }
    } catch (e) {
      console.error(`Error when parsing pixelplanet url ${e}`);
    }
  }

  async recMatrix(request, context) {
    const event = request.getData();
    // user joined or set displayname
    if (event.type === "m.room.member"
      && event.user_id
      && event.content?.displayname
    ) {
      const { displayname } = event.content;
      const userId = event.user_id;
      const name = await this.getNameForPP(userId, displayname);
      console.log(`User ${userId} joined or set displayname to ${name}`);
      this.idToNameMap.set(userId, name);
      return;
    }
    // Only room messages from bridged rooms past this point
    if (event.type !== "m.room.message" || !event.content) {
      return;
    }
    const cid = this.mToProomMap.get(event.room_id);
    if (!cid) {
      return;
    }
    let msg;
    // Media: send url as markdown
    if ((event.content.msgtype === "m.image"
      || event.content.msgtype === "m.video")
      && event.content.url
    ) {
      // adding a query parameter so that client can guess what it is
      const url = `${this.mediaUrl}/_matrix/media/r0/download/${event.content.url.substring("mxc://".length)}?type=${event.content.msgtype.substring(2)}`
      msg = `[${event.content.body}](${url})`;
    // Text
    } else if (event.content.msgtype === "m.text") {
      msg = event.content.body;
      this.sendCanvasSnapshotIfNeccessary(event.room_id, msg);
    }
    if (!msg) {
      return;
    }

    const userId = event.sender;

    // block none :pixelplanet.fun users
    if (!userId.endsWith(this.domain)) {
      return;
    }

    const uid = (userId.startsWith(`@${this.prefix}_`)
      && userId.endsWith(this.domain))
      ? userId.slice(2 + this.prefix.length, -this.domain.length - 1)
      : null;
    if (this.checkEchoSuppression(uid, cid)) {
      return;
    }
    let name = this.idToNameMap.get(userId);
    if (!name) {
      name = await this.getNameForPP(userId);
      this.idToNameMap.set(userId, name);
    }
    if (name === 'event' || name === 'info') {
      // don'tallow special names used by system
      return;
    }
    console.log(`MATRIX ${name}: ${msg}`);
    splitString(msg).forEach(
      (line) => this.sendPPfun(name, parseInt(uid, 10), line, cid),
    );
  }

  /*
   * get name to use for pixelplanet messages
   */
  async getNameForPP(userId, displayname) {
    // is ppfun user
    if (userId.startsWith(`@${this.prefix}_`) && userId.endsWith(this.domain)) {
      return `[mx] ${displayname || await this.getDisplayName(userId)}`;
    }
    return userId.substring(1);
  }

  /*
   * get displayname of matrix user
   */
  async getDisplayName(userId) {
    try {
      const intent = this.matrixBridge.getIntent();
      const profile = await intent.getProfileInfo(userId, 'displayname');
      const { displayname } = profile;
      return displayname;
    } catch (e) {
      console.error(`Could not fetch DiplayName for ${userId}: ${e}`);
      return userId.substring(1);
    }
  }

  async run() {
    console.log('STARTING MATRIX CONNECTION');
    await this.matrixBridge.run(this.port, null, '127.0.0.1');
    console.log('STARTING PPFUN CONNECTION');
    await this.ppfunSocket.run();
    console.log('BRIDGE CONNECTED');
    this.isConnected = true;
  }

  /*
   * uid needs to be a number
   */
  sendPPfun(name, uid, msg, cid) {
    const msgString = msg.trim();
    if (!msgString.length) {
      return;
    }
    this.ppfunSocket.emit(
      'sendChatMessage',
      name,
      uid || null,
      msgString,
      cid,
    );
  }

  /*
   * send message to matrix
   * This also registers a new user if none exist
   */
  async sendMatrix(name, userId, msg, roomId) {
    try {
      const intent = this.matrixBridge.getIntent(userId);
      await intent.ensureProfile(name);
      intent.sendText(roomId, msg);
    } catch (err) {
      console.warn(`Got error on sending message to matrix: ${err.message}`);
    }
  }

  /*
   * make a user admin in a room
   */
  async makeAdminInRoom(userId, roomId) {
    if (userId.startsWith(`@${this.prefix}_`)) {
      try {
        const userIntent = this.matrixBridge.getIntent(userId);
        await userIntent.join(roomId);
      } catch (err) {
        console.warn(`Couldn't join room ${roomId} with user ${userId}: ${err.message}`);
      }
    }
    try {
      const intent = this.matrixBridge.getIntent();
      await intent.setPowerLevel(roomId, userId, 100);
    } catch (err) {
      console.warn(`Couldn't give ${userId} admin rights for ${roomId}: ${err.message}`);
    }
  }

  /*
   * creates matrix rooms for the pixelplanet channels if neccessary and
   * opulates the mToProomMap and pToMroomMap.
   * @param roomList list of rooms and their names like:
   *   [[roomId, roomName], [roomId2, roomName2], ...]
   * @return Promise<int> number of how many rooms got created
   */
  async connectRooms(roomList) {
    try {
      const intent = this.matrixBridge.getIntent();
      this.syncChannels = false;
      for (let i = 0; i < roomList.length; i += 1) {
        const [ppfun_id, ppfun_name] = roomList[i];
        const localpart = `${this.prefix}_${ppfun_name}`
        const alias = `#${localpart}:${this.domain}`;
        let room_id = null;
        try {
          console.log('resolving', ppfun_id, ppfun_name, alias);
          room_id = await intent.resolveRoom(alias);
        } catch {
          console.log(`Matrix room ${alias} does not exist. Try to create it`);
          /*
           * see https://spec.matrix.org/v1.1/client-server-api/
           * look for requestBody of createRoom
           */
          try {
            room_id = await intent.createRoom({
              options: {
                name: `[${ppfun_name}] pixelplanet.fun`,
                topic: `This is the official room for the ${ppfun_name} channel on pixelplanet.fun. A pixel drawing game.`,
                room_alias_name: localpart,
                visibility: 'public',
                preset: 'public_chat',
              },
            });
            room_id = room_id.room_id;
            await intent.setRoomAvatar(room_id, 'mxc://pixelplanet.fun/cXJRbbaaqkzpOydGVxASpizK');
          } catch (e) {
            console.log(`Could not create new room for ${alias}: ${e}`);
          }
        }
        if (room_id) {
          await this.makeAdminInRoom(`@${this.prefix}_admin:pixelplanet.fun`, room_id);
          await this.makeAdminInRoom('@hf:pixelplanet.fun', room_id);
          console.log(`Mapped ${ppfun_name} | ${ppfun_id} to ${alias} | ${room_id}`);
          this.mToProomMap.set(room_id, ppfun_id);
          this.pToMroomMap.set(ppfun_id, room_id);
        }
      }
    } catch (e) {
      console.error(`Could not sync matrix rooms to ppfun channels: ${e}`);
    }
  }
}

export default PPfunMatrixBridge;
