/*
 * Markdown parsing
 *
 * Parses the very basics that are needed for the bridge
 */

import MString from './MString.js';

const paraElems = ['*', '~', '+', '_'];
function parseMParagraph(text) {
  const pArray = [];
  let pStart = text.iter;
  let chr = null;
  while (!text.done()) {
    chr = text.getChar();

    if (chr === '\n') {
      text.moveForward();
      break;
    }

    if (chr === '\\') {
      /*
       * escape character
       */
      if (pStart !== text.iter) {
        pArray.push(text.slice(pStart));
      }
      pStart = text.iter + 1;
      text.moveForward();
    } else if (chr === '#') {
      /*
       * ppfun coords #d,34,23,-10
       */
      const oldPos = text.iter;
      const coords = text.checkIfCoords();
      if (coords) {
        if (pStart !== oldPos) {
          pArray.push(text.slice(pStart, oldPos));
        }
        pArray.push(['l', null, `https://pixelplanet.fun/${coords}`]);
        pStart = text.iter;
      }
    } else if (paraElems.includes(chr)) {
      /*
       * bold, cursive, underline, etc.
       */
      if (pStart !== text.iter) {
        pArray.push(text.slice(pStart));
      }
      pStart = text.iter + 1;
    } else if (chr === ':') {
      /*
       * pure link
       */
      const link = text.checkIfLink();
      if (link !== null) {
        const startLink = text.iter - link.length;
        if (pStart < startLink) {
          pArray.push(text.slice(pStart, startLink));
        }
        pArray.push(['l', null, link]);
        pStart = text.iter;
        continue;
      }
    } else if (chr === '[') {
      /*
       * x[y](z) enclosure
       */
      let oldPos = text.iter;
      let x = null;
      if (text.iter > 0) {
        text.move(-1);
        x = text.getChar();
        text.setIter(oldPos);
      }
      /*
       * x decides what element it is
       * defaults to ordinary link
       */
      let tag = 'l';
      let zIsLink = true;
      if (x === '!') {
        tag = 'img';
        oldPos -= 1;
      } else if (x === '@') {
        zIsLink = false;
        tag = '@';
        oldPos -= 1;
      }

      const encArr = text.checkIfEnclosure(zIsLink);
      if (encArr !== null) {
        if (pStart < oldPos) {
          pArray.push(text.slice(pStart, oldPos));
        }
        pArray.push([tag, encArr[0], encArr[1]]);
        pStart = text.iter + 1;
      }
    }

    text.moveForward();
  }
  if (pStart !== text.iter) {
    pArray.push(text.slice(pStart));
  }
  return pArray;
}

export function parseParagraph(text,) {
  const mText = new MString(text);
  return parseMParagraph(mText);
}
