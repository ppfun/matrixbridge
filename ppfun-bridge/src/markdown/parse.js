import { parseParagraph } from './MarkdownParserLight.js';

export default function parseMsg(msg) {
  const pArray = parseParagraph(msg);
  let output = '';
  for (let i = 0; i < pArray.length; i += 1) {
    const part = pArray[i];
    if (!Array.isArray(part)) {
      output += part;
    } else {
      const type = part[0];
      switch (type) {
        case '@': {
          output += `@${part[1]}`;
          break;
        }
        case 'img':
        case 'l': {
          output += part[2];
          break;
        }
        default:
          output += type;
      }
    }
  }
  return output;
}
