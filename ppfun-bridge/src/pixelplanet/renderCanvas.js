import sharp from 'sharp';

import getChunk from './loadChunk.js';
import {
  WIDTH,
  HEIGHT,
  MAX_SCALE,
  TILE_SIZE,
  TILE_ZOOM_LEVEL,
  BACKGROUND_CLR_RGB,
} from './constants.js';

function coordToChunk(z, canvasSize, tiledScale) {
  return Math.floor((z + canvasSize / 2) / TILE_SIZE * tiledScale);
}

function chunkToCoord(z, canvasSize, tiledScale) {
  return Math.round(z * TILE_SIZE / tiledScale - canvasSize / 2);
}

async function fillRect(
  buffer, width, height,
  x, y,
  w, h,
  r, g, b,
) {
  if (x < 0) {
    w += x;
    x = 0;
  }
  if (y < 0) {
    h += y;
    y = 0;
  }
  if (w + x > width) {
    w = width - x;
  }
  if (h + y > height) {
    h = height - y;
  }
  const rowMax = y + h;
  for (let row = y; row < rowMax; row += 1) {
    let pos = (row * width + x) * 3;
    const max = pos + w * 3;
    while (pos < max) {
      buffer[pos++] = r;
      buffer[pos++] = g;
      buffer[pos++] = b;
    }
  }
}

async function drawChunk(
  buffer, width, height,
  xOff, yOff,
  canvas, tiledZoom, xc, yc,
) {
  const chunkBuffer = await getChunk(canvas, tiledZoom, xc, yc);
  const [ emptyR, emptyG, emptyB ] = canvas.palette.rgb;
  let row = Math.max(-yOff,  0);
  const rowMax = (TILE_SIZE + yOff > height) ? height - yOff : TILE_SIZE;
  const colMin = Math.max(-xOff, 0);
  const colMax = (TILE_SIZE + xOff > width) ? width - xOff : TILE_SIZE;
  if (colMax < 0 || rowMax < 0) {
    // out of image bounds
    return;
  }
  const cutWidth = colMax - colMin;
  for (; row < rowMax; row += 1) {
    let ib = (row * TILE_SIZE + colMin) * 3;
    let pos = ((row + yOff) * width + colMin + xOff) * 3;
    const max = pos + cutWidth * 3;
    while (pos < max) {
      if (ib < chunkBuffer.byteLength) {
        buffer[pos++] = chunkBuffer[ib++];
        buffer[pos++] = chunkBuffer[ib++];
        buffer[pos++] = chunkBuffer[ib++];
      } else {
        buffer[pos++] = emptyR;
        buffer[pos++] = emptyG;
        buffer[pos++] = emptyB;
      }
    }
  }
}

/**
 * Draw canvas section from given coordinates
 * @param canvas canvas Data
 * @param x x coordinates of center of wanted image
 * @param y y coordinates of center of wanted image
 * @param z wanted zoomlevel in log2 * 10
 * @param width width of image (optional)
 * @param height height of image (optional)
 */
export default async function renderCanvas(
  title,
  canvas,
  x,
  y,
  z,
  width = WIDTH,
  height = HEIGHT,
) {
  const canvasSize = canvas.size;

  let scale = 2 ** (z / 10);
  scale = Math.max(scale, TILE_SIZE / canvas.size);
  scale = Math.min(scale, MAX_SCALE);

  // 2 + 2 is 4 minus 1 that's 3, Quick Mafs!
  let tiledScale = (scale > 0.5)
    ? 0
    : Math.round(Math.log2(scale) / Math.log2(TILE_ZOOM_LEVEL));
  tiledScale = TILE_ZOOM_LEVEL ** tiledScale;
  const tiledZoom = canvas.maxTiledZoom
    + Math.log2(tiledScale) / Math.log2(TILE_ZOOM_LEVEL);
  const idRelScale = scale / tiledScale;
  // split relative scale into vertical and horizontal scale that rounds to full integer
  const unscaledWidth = Math.round(width / idRelScale);
  const unscaledHeight = Math.round(height / idRelScale);
  const relScaleW = width / unscaledWidth;
  const relScaleH = height / unscaledHeight;
  // canvas coordinates of corners
  const tlX = Math.floor(x - width / 2 / scale);
  const tlY = Math.floor(y - height / 2 / scale);
  const brX = Math.floor(x - 1 + width / 2 / scale);
  const brY = Math.floor(y - 1 + height / 2 / scale);
  // chunk coordinates of chunks in corners
  const tlCX = coordToChunk(tlX, canvasSize, tiledScale);
  const tlCY = coordToChunk(tlY, canvasSize, tiledScale);
  const brCX = coordToChunk(brX, canvasSize, tiledScale);
  const brCY = coordToChunk(brY, canvasSize, tiledScale);
  console.log(`Load chunks from ${tlCX} / ${tlCY} to ${brCX} / ${brCY}`);
  
  // create RGB buffer for unscalled pixels and load chunks into it
  const pixelBuffer = Buffer.allocUnsafe(unscaledWidth * unscaledHeight * 3);
  const chunkMax = canvas.size / TILE_SIZE * tiledScale;
  const promises = [];
  for (let xc = tlCX; xc <= brCX; xc += 1) {
    for (let yc = tlCY; yc <= brCY; yc += 1) {
      const xOff = Math.round((chunkToCoord(xc, canvasSize, tiledScale) - tlX) * tiledScale);
      const yOff = Math.round((chunkToCoord(yc, canvasSize, tiledScale) - tlY) * tiledScale);
      if (xc < 0 || xc >= chunkMax || yc < 0 || yc >= chunkMax) {
        // out of canvas bounds
        console.log(`Chunk ${xc}, ${yc} out of canvas bounds`);
        promises.push(
          fillRect(
            pixelBuffer, unscaledWidth, unscaledHeight,
            xOff, yOff, TILE_SIZE, TILE_SIZE,
            ...BACKGROUND_CLR_RGB,
          ),
        );
      } else {
        promises.push(
          drawChunk(
            pixelBuffer, unscaledWidth, unscaledHeight,
            xOff, yOff,
            canvas, tiledZoom, xc, yc,
          ),
        );
      }
    }
  }
  await Promise.all(promises);
  // scale and convert to png
  const imageBuffer = await sharp(pixelBuffer, {
      raw: {
        width: unscaledWidth,
        height: unscaledHeight,
        channels: 3,
      },
    })
    .resize({ width, height, kernel: 'nearest' })
    .png()
    .toBuffer();

  return {
    image: imageBuffer,
    name: `ppfun-snap-${title}.png`,
    type: 'image/png',
    w: width,
    h: height,
    size: imageBuffer.size,
  }
}
