import fetch from 'node-fetch';
import sharp from 'sharp';

import {
  TILE_SIZE,
} from './constants.js';

async function fetchBaseChunk(
  canvasId,
  palette,
  zoom,
  cx, cy,
) {
  const url = `https://pixelplanet.fun/chunks/${canvasId}/${cx}/${cy}.bmp`;
  console.log(`Fetching ${url}`);
  const response = await fetch(url);
  if (response.ok) {
    const arrayBuffer = await response.arrayBuffer();
    if (arrayBuffer.byteLength) {
      const chunkArray = new Uint8Array(arrayBuffer);
      return palette.buffer2RGB(chunkArray);
    }
  }
  throw new Error(`Chunk faulty or not found`);
}

async function fetchTile(canvasId, zoom, cx, cy) {
  const url = `https://pixelplanet.fun/tiles/${canvasId}/${zoom}/${cx}/${cy}.webp`;
  console.log(`Fetching ${url}`);
  const response = await fetch(url);
  if (response.ok) {
    const arrayBuffer = await response.arrayBuffer();
    if (arrayBuffer.byteLength) {
      return sharp(arrayBuffer).removeAlpha().raw().toBuffer();
    }
  }
  throw new Error(`Chunk faulty or not found`);
}

/**
 * fetch a tile
 * @param canvas canvas object
 * @param zoom tile zoom level
 * @param cx cavnas coord x
 * @param cy cavnas coord y
 * @return RGB Buffer of chunk (NOT padded)
 */
export default async function (
  canvas,
  zoom,
  cx,
  cy,
) {
  const canvasId = canvas.id;
  try {
    if (canvas.maxTiledZoom === zoom) {
      return await fetchBaseChunk(canvasId, canvas.palette, zoom, cx, cy);
    } else {
      return await fetchTile(canvasId, zoom, cx, cy);
    }
  } catch (err) {
    console.log(`Chunk ${cx} / ${cy} - ${zoom}: ${err.message}`);
    return Buffer.allocUnsafe(0);
  }
}
