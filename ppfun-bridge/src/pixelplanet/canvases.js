import Palette from './Palette.js';
import fetch from 'node-fetch';
import {
  TILE_SIZE,
  TILE_ZOOM_LEVEL,
} from './constants.js';

let canvases = new Map();

async function fetchCanvasData() {
  try {
    const response = await fetch('https://pixelplanet.fun/api/me');
    if (response.status >= 300) {
      throw new Error('Can not connect to pixelplanet!');
    }
    const data = await response.json();

    const ids = Object.keys(data.canvases);
    for (let i = 0; i < ids.length; i += 1) {
      const id = ids[i];
      const canvas = data.canvases[id];
      canvas.id = id;
      canvas.palette = new Palette(canvas.colors);
      canvas.maxTiledZoom = Math.log2(canvas.size / TILE_SIZE)
        / Math.log2(TILE_ZOOM_LEVEL);
      canvases.set(canvas.ident, canvas);
    }
    console.log('Successfully fetched canvas data from pixelplanet');
  } catch (e) {
    console.log('Couldn\'t connect to pixelplanet, trying to connect again in 60s');
    setTimeout(fetchCanvasData, 60000);
    throw(e)
  }
}
fetchCanvasData();

export default canvases;
