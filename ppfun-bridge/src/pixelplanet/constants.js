export const WIDTH = 800;
export const HEIGHT = 600;
export const MAX_SCALE = 40; // 52 in log2
export const TILE_SIZE = 256;
export const TILE_ZOOM_LEVEL = 2;
export const BACKGROUND_CLR_RGB = [ 196, 196, 196 ];
export const QUEUE_LIMIT = 3;
