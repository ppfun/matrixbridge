import fetch from 'node-fetch';

import canvases from './canvases.js';
import renderCanvas from './renderCanvas.js';
import { QUEUE_LIMIT }  from './constants.js';

const linkRegExp = /(#[a-z]*,-?[0-9]*,-?[0-9]*(,-?[0-9]+)?)/gi;
const linkRegExpFilter = (val, ind) => ((ind % 3) !== 2);
let lastCoords = [];
let lastParse = Date.now();

function compArray(arr1, arr2) {
  for (let i = 0; i <= arr1.length; i += 1) {
    if (arr1[i] !== arr2[i]) {
      return false;
    }
  }
  return true;
}

/*
 * render canvas snapshots sequentially in a queue,
 * don't repeat most recent render
 */
const queue = [];
let rendering = false;
let lastTitle = '';

async function render() {
  if (!queue.length) {
    rendering = false;
    return;
  }
  rendering = true;
  const [args, resolve, reject] = queue.shift();
  try {
    const ret = await renderCanvas(...args);
    resolve(ret);
  } catch (err) {
    reject(err)
  }
  setTimeout(render, 100);
}

function renderCanvasQueue(...args) {
  if (queue.length >= QUEUE_LIMIT || args[0] === lastTitle) {
    return null;
  }
  return new Promise((resolve, reject) => {
    lastTitle = args[0];
    queue.push([args, resolve, reject]);
    if (!rendering) {
      setImmediate(render);
    }
  });
}

/**
 * check if canvas link is in message and return screenshot of the
 * current canvas if so
 * @param text message text
 * @return object with image, name, type, w, h, size
 */
export async function parseCanvasLinks(text) {
  if (!text || !canvases) {
    return null;
  }

  const msgArray = text.split(linkRegExp).filter(linkRegExpFilter);
  if (msgArray.length <= 1) {
    return null;
  }

  const coordsParts = msgArray[1].substr(1).split(',');
  const curTime = Date.now();
  if (compArray(coordsParts, lastCoords)
    || curTime - 4000 < lastParse
  ) {
    return null;
  }
  lastParse = curTime;
  lastCoords = coordsParts;
  const [ canvasIdent, x, y ]  = coordsParts;
  const z = coordsParts[3] || 16;

  const canvas = canvases.get(canvasIdent);
  const upperBound = canvas.size / 2 - 1;
  const lowerBound = canvas.size / -2;
  if (!canvas
    || x > upperBound
    || y > upperBound
    || x < lowerBound
    || y < lowerBound
  ) {
    return;
  }
  
  const title = `${canvas.title}_${x}_${y}_${z}`;
  console.log(`Fetch canvas ${title}`);
  const imageData = await renderCanvasQueue(title, canvas, x, y, z);

  return imageData;
}
