#!/bin/bash
scp -r ./src/* pixelplanet:~/ppfun-bridge/src/
scp ./*.js pixelplanet:~/ppfun-bridge/
scp ./*.json pixelplanet:~/ppfun-bridge/
ssh pixelplanet 'pm2 stop ppfun-bridge; cd ~/ppfun-bridge; pm2 start ecosystem.yml'
