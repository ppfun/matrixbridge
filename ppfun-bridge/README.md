# ppfun-bridge Matrix Application Service

Sends chat messages from ppfun to matrix and vice versa.

Uses the matrix [Application Service API](https://spec.matrix.org/v1.1/application-service-api/) and pixelplanet.fun admin-API (ApiWebsocket and ./adminapi, so admin-access and set APISOCKET_KEY is required).

It is supposed to run on the same server as [matrix-synapse](https://matrix.org/docs/projects/server/synapse).

The Application Service communicaties with matrix via HTTP and listens itself on the port 8009.

## Running

Install dependencies

```
npm install
```

Edit `ppfun-registration.yaml` keys `hs_key` and `as_key` into some random strings and move the file somewhere where both the ppfun-bridge and matrix-synapse can access it. Don't change the `pp_` aliases, because they are hardcoded in the module and bridge.

Edit your `homeserver.yml` matrix-synapse config file and add a path to the `ppfun-registration.yml` like this:

```
app_service_config_files:
  - /etc/matrix-synapse/ppfun-registration.yaml
```

Edit ecosystem.yml and set the path to the `ppfun-registration.yml` as REGISTRATION_YAML.
Set the pixelplanet APISOCKET_KEY and APISOCKET_URL (like `https://pixelplanet.fun/mcws`).
HOMESERVER_URL should be the local url to matrix-synapse like `http://localhost:8008` and HOMESERVER_DOMAIN its base_url / server_name like `pixelplanet.fun`
MEDIA_URL is the http[s] url from which the matrix server is reachable from the outside, which is usually the base_url, it is needed to send links

Now you can start the brige with pm2:

```
pm2 start ecosystem.yml
```

and stop it with

```
pm2 stop ppfun-bridge
```

and watch its logs with

```
pm2 log ppfun-bridge
```

## Room Creation

The bridge automatically creates matrix rooms for all public pixelplanet channels available and makes the '@pp_admin:pixelplanet.fun' user admin.
Some things are hardcoded in `src/ppfunMatrixBridge.js`, check it out if there are problems.
And the URL where to fetch chunks from pixelplanet is hardcoded in `src/pixelplanet/loadChunk.js`.


## References

 - its based on [matrix-appservice-bridge](https://github.com/matrix-org/matrix-appservice-bridge/blob/develop/HOWTO.md)
 - [matrix-appservice-bridge docs](https://matrix-org.github.io/matrix-appservice-bridge/3.2.0/classes/Bridge.html#run)
 - [matrix-appservice-bridge example](https://github.com/matrix-org/matrix-appservice-bridge/blob/develop/HOWTO.md)
