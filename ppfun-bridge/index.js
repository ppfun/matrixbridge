"use strict";
// Usage:
// node index.js -r -u "http://localhost:9000" # remember to add the registration!
// node index.js -p 9000

import PPfunMatrixBridge from './src/ppfunMatrixBridge.js';

const PORT = parseInt(process.env.PORT, 10) || 8009;
const APISOCKET_KEY = process.env.APISOCKET_KEY || '';
const APISOCKET_URL = process.env.APISOCKET_URL || 'wss://dev.pixelplanet.fun/mcws';
const REGISTRATION_YAML = process.env.REGISTRATION_YAML || '/etc/matrix-synapse/ppfun-registration.yaml';
const HOMESERVER_URL = process.env.HOMESERVER_URL || 'http://localhost:8008';
const HOMESERVER_DOMAIN = process.env.HOMESERVER_DOMAIN || 'pixelplanet.fun';
const MEDIA_URL = process.env.MEDIA_URL || `https://${HOMESERVER_DOMAIN}`;

const lmao = new PPfunMatrixBridge({
  apiSocketKey: APISOCKET_KEY,
  apiSocketUrl: APISOCKET_URL,
  homeserverUrl: HOMESERVER_URL,
  domain: HOMESERVER_DOMAIN,
  registration: REGISTRATION_YAML,
  port: PORT,
  mediaUrl: MEDIA_URL
});

lmao.run();
