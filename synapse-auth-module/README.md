# ppfun-auth-module for matrix-synapse

Allows pixelplanet users to login with their credentials on matrix.
Required synapse version is 1.69+ 

## Installation

Put `ppfun_auth.py` into the python PATH of synapse, sometimes it is `/usr/local/lib/python3.8/site-packages/ppfun_auth.py` or in Debian / Ubuntu distribution in `/opt/venvs/matrix-synapse/...`.

## Configuration (`homeserver.yml`):

```yaml
modules:
  - module: ppun_auth.PPfunAuthProvider
    config:
      apisocketkey: "APISOCKETKEY_CONFIGURED_IN_PIXELPLANET"
      ppfunurl: "http://local.pixelplanet.url:port"
      # if true, only mail-verified users can log in
      verified: true
      # rooms that will be automatically joined on login, must be a list 
      autojoin_rooms: ['#pp_en:pixelplanet.fun', '#pp_int:pixelplanet.fun']
      # dont notify users in pp_ channels
      # when true, it triggers a monkeypatch that blocks notifications in bridged channels in order to avoid flooding the synapse database with push_actions
      block_notify: false
```
## References

 - [synapse documentation about Pluggable Modules](https://matrix-org.github.io/synapse/latest/modules/index.html)
 - [synapse ModuleAPI class](https://github.com/matrix-org/synapse/blob/master/synapse/module_api/__init__.py)
 - [matrix-synapse-ldap3](https://github.com/matrix-org/matrix-synapse-ldap3/blob/main/ldap_auth_provider.py) as a different example
